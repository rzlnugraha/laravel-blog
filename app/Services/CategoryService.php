<?php

namespace App\Services;

use App\Exceptions\CustomException;
use App\Models\Category;

class CategoryService
{
    protected $category;

    public function __construct(Category $category)
    {
        $this->category = $category;
    }

    public function get($columns = '*')
    {
        try {
            return $this->category->with('articles')->select($columns)->latest()->get();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function paginate($paginate = 5, $columns = '*')
    {
        try {
            return $this->category->with('articles')->select($columns)->latest()->paginate($paginate);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function create(array $data)
    {
        try {
            return $this->category->create($data);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function update(int $id, array $data)
    {
        try {
            return $this->find($id)->update($data);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function find($id)
    {
        try {
            return $this->category->find($id);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function findByColumn($column, $value)
    {
        try {
            return $this->category->with('articles')->where($column, $value)->first();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            return $this->category->destroy($id);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }
}

