<script>
    $(document).ready(function () {
        const url = "{{ route('admin.category.datatable') }}";
        const form = $('#form-category')
        const table = $('#dataTable');
        const modal = $('#modal-category')
        const btnCreate = $('.btn-create')
        const dataTable = table.DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'slug', name: 'slug'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        /* Functions */
        const saveCategory = (data) => {
            const url = '{{ route("admin.category.store") }}'
            $.post(url, data, (res) => {
                clearModalForm()
                dataTable.ajax.reload();

                swalSuccess('Success');
            }).fail(err => {
                switch (err.status) {
                    case 422:
                        let message = err.responseJSON.data.join('<br>')
                        swalError(message)
                        break;
                    case 500:
                        swalError('Something went wrong')
                    default:
                        break;
                }
            })
        }

        const getCategoryById = (id) => {
            let url = "{{ route('admin.category.edit', 'id') }}"
                url = url.replace('id', id)

            $.get(url, function (res) {
                modal.find('#id').val(res.data.id)
                modal.find('#name').val(res.data.name)

                modal.modal('show')
                modal.find('.modal-title').text('Edit Category')
                modal.find('.button-save').attr('data-type', 'update')
            })
        }

        const updateCategory = (id, data) => {
            let url = "{{ route('admin.category.update', 'id') }}"
                url = url.replace('id', id)

            $.ajax({
                url: url,
                method: 'PUT',
                data: data,
                success: (res) => {
                    clearModalForm()
                    dataTable.ajax.reload();

                    swalSuccess('Success');
                },
                error: (err) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const deleteCategory = (id) => {
            let url = "{{ route('admin.category.delete', 'id') }}"
                url = url.replace('id', id)

            $.ajax({
                url: url,
                method: 'DELETE',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: (res) => {
                    dataTable.ajax.reload();

                    swalSuccess('Success');
                },
                error: (err) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const clearModalForm = () => {
            modal.find('.form-control').val('')
            modal.modal('hide')
        }

        btnCreate.click(function () {
            modal.find('.form-control').val('')
            modal.find('.modal-title').text('Create Category')
            modal.find('.button-save').attr('data-type', 'save')
            modal.modal('show')
        })

        form.submit(function (e) {
            e.preventDefault();

            const data = $(this).serialize()
            let id = modal.find('#id').val()

            if ($('.button-save').attr('data-type') == 'save') return saveCategory(data)

            return updateCategory(id, data)
        })

        $(document).on('click', '.btn-edit', function () {
            let self = $(this)
            let id = self.data('id')

            getCategoryById(id)
        })

        $(document).on('click', '.btn-delete', function () {
            if (confirm('Apakah anda yakin?')) {
                const id = $(this).data('id')

                deleteCategory(id);
            }
        })
    });

</script>
