<?php

namespace App\Services;

use App\Models\User;
use App\Exceptions\CustomException;

class UserService
{
    protected $user;

    public function __construct(User $user)
    {
        $this->user = $user;
    }

    public function get($columns = '*')
    {
        try {
            return $this->user->select($columns)->with('role')->latest()->get();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function create($data)
    {
        try {
            $data['password'] = bcrypt($data['password']);

            return $this->user->create($data);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function update(int $id, array $data)
    {
        try {
            $data = collect($data);

            if (!$data->has('old_password')) {
                $data->forget(['password']);
            } else {
                $data['password'] = bcrypt($data['password']);
            }

            return $this->find($id)->update($data->all());
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function find($id)
    {
        try {
            return $this->user->find($id);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            $user = $this->user->find($id);
            $user->articles()->delete();
            return $user->delete();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }
}

