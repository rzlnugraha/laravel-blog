<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\JsonResponse;
use App\Http\Requests\ArticleRequest;
use App\Services\ArticleService;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Str;
use DataTables;

class ArticleController extends Controller
{
    protected $articleService;
    protected $jsonResponse;

    public function __construct(ArticleService $articleService, JsonResponse $jsonResponse)
    {
        $this->articleService = $articleService;
        $this->jsonResponse = $jsonResponse;
    }

    public function index()
    {
        $categories = resolve(CategoryService::class)->get();
        return view('cms.article.index', compact('categories'));
    }

    public function store(ArticleRequest $request)
    {
        try {
            $request->merge([
                'slug' => Str::slug($request->title)
            ]);
            $this->articleService->create($request->all());

            return $this->jsonResponse->data([])->message('Successfully create article data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function update($id, ArticleRequest $request)
    {
        try {
            $request->merge([
                'slug' => Str::slug($request->title)
            ]);

            $this->articleService->update($id, $request->all());

            return $this->jsonResponse->message('Successfully update article data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function edit($id)
    {
        try {
            $article = $this->articleService->find($id);

            return $this->jsonResponse->data($article)->message('Successfully update article data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function show($slug)
    {
        $article = $this->articleService->findByColumn('slug', $slug);
        return view('cms.article.detail', compact('article'));
    }

    public function delete($id)
    {
        try {
            $this->articleService->delete($id);

            return $this->jsonResponse->message('Successfully delete article data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function datatable()
    {
        $categories = $this->articleService->getByRole();

        return DataTables::of($categories)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $detailPage = route('admin.article.detail', $row->slug);

                $btn = "<div class='d-flex justify-content-center align-items-center'>";
                $btn .= "<button title='Edit' class='btn btn-warning btn-edit btn-sm mr-2' data-id='{$row->id}'><i class='fa fa-pen'></i></button>";
                $btn .= "<a href='$detailPage' title='Detail' class='btn btn-info btn-edit btn-sm mr-2'><i class='fa fa-info-circle'></i></a>";
                $btn .= "<button data-id='{$row->id}' title='Delete' class='btn btn-danger btn-sm btn-delete'><i class='fa fa-trash'></i></button>";
                $btn .= '</div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
