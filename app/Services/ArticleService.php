<?php

namespace App\Services;

use App\Exceptions\CustomException;
use App\Models\Article;
use Image;
use File;

class ArticleService
{
    protected $article;

    public function __construct(Article $article)
    {
        $this->article = $article;
        $this->articleImagePath = public_path('images/article');
        $this->articleImageThumbnailPath = public_path('images/thumbnail/article');
    }

    public function get($columns = '*')
    {
        try {
            return $this->article->with('category', 'user')->select($columns)->latest()->get();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function getByRole()
    {
        try {
            $isAdmin = isAdmin();

            return $this->article
                ->with('category', 'user')
                ->when(!$isAdmin, function ($query) {
                    return $query->where('created_by', userLogin()->id);
                })
                ->latest()
                ->get();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function paginate($paginate = 5, $columns = '*')
    {
        try {
            return $this->article->with('category', 'user')->select($columns)->latest()->paginate($paginate);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage());
        }
    }

    public function create($data)
    {
        try {
            if (!empty($data['banner'])){
                $data['banner'] = $this->uploadImage($data['banner']);
            }

            $data['created_by'] = userLogin()->id;

            return $this->article->create($data);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function update(int $id, array $data)
    {
        try {
            if (!empty($data['banner'])){
                $data['banner'] = $this->uploadImage($data['banner']);
            }

            return $this->find($id)->update($data);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function find($id)
    {
        try {
            return $this->article->find($id);
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function take($limit = 5)
    {
        try {
            return $this->article->with('category')->latest()->take($limit)->get();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function findByColumn($column, $value)
    {
        try {
            return $this->article->with('category')->where($column, $value)->first();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            $article = $this->article->find($id);

            $this->removeImage($article->banner);

            return $article->delete();
        } catch (\Throwable $th) {
            throw new CustomException($th->getMessage(), 500);
        }
    }

    private function uploadImage($image)
    {
        /* Create new folder for thumbnail path if folder doesnt exists */
        if (!file_exists($this->articleImageThumbnailPath)) {
            mkdir($this->articleImageThumbnailPath, 666, true);
        }

        $imageName = rand(1111,9999) . date('Ymd') . '.' . $image->getclientOriginalExtension();

        /* Start Resize Image for Thumbnail */
        $img = Image::make($image->path());
        $img->resize(100, 100, function ($constraint) {
            $constraint->aspectRatio();
        })->save($this->articleImageThumbnailPath . '/' . $imageName);
        /* End Resize Image for Thumbnail */

        $save = $image->move($this->articleImagePath, $imageName);

        if (!$save || !$img) throw new CustomException('Upload image failed', 400);

        return $imageName;
    }

    public function removeImage($image)
    {
        if (File::exists($this->articleImagePath . '/' . $image)) {
            File::delete($this->articleImagePath . '/' .  $image);
        }
        if (File::exists($this->articleImageThumbnailPath . '/' . $image)) {
            File::delete($this->articleImageThumbnailPath . '/' . $image);
        }

        return;
    }
}

