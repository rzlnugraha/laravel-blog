<?php

namespace App\Http\Controllers;

use App\Services\ArticleService;
use Illuminate\Http\Request;

class HomepageController extends Controller
{
    public function index()
    {
        $articles = resolve(ArticleService::class)->take(5);

        return view('web.homepage.index', compact('articles'));
    }
}
