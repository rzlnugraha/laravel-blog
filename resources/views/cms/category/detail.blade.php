@extends('layouts.app')
@section('title')
    Laravel Blog - Category
@endsection

@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">{{ $category->name }}</h6>
        </div>
        <div class="card-body">
            <table class="table">
                <thead class="thead-dark">
                  <tr>
                    <th scope="col">No</th>
                    <th scope="col">Article Name</th>
                    <th scope="col">Created At</th>
                    <th scope="col">#</th>
                  </tr>
                </thead>
                <tbody>
                    @forelse ($category->articles as $article)
                        <tr>
                            <td>{{ $loop->iteration }}</td>
                            <td>{{ $article->title }}</td>
                            <td>{{ $article->created_at->format('Y-m-d H:i:s') }}</td>
                            <td><a class="btn btn-sm btn-info" href="{{ route('admin.article.detail', $article->slug) }}" title="Go To {{ $article->title }}"><i class="fa fa-info-circle"></i></a></td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="4" align="center"><b>Articles Not Found</b></td>
                        </tr>
                    @endforelse
                </tbody>
              </table>
        </div>
    </div>
</div>
@endsection
