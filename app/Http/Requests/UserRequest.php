<?php

namespace App\Http\Requests;

use Illuminate\Contracts\Validation\Validator;
use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Validation\ValidationException;

class UserRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = $this->id;

        return [
            'name' => 'required',
            'email' => "required|email|unique:users,email,$id",
            'role_id' => 'required|exists:roles,id'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = response()->json([
            'meta' => [
                'message' => 'something wrong !',
                'code' => 422,
            ],
            'data' => $validator->errors()->all(),
        ], 422);

        throw new ValidationException($validator, $response);
    }
}
