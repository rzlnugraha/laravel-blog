@extends('web.layouts.layouts')

@section('content')
    <!-- Page Content -->
<div class="container">

    <!-- Portfolio Item Heading -->
    <h5 class="my-4">{{ $article->title }}</h5>
    <small>Created By : {{ $article->user->name }}</small>

    <!-- Portfolio Item Row -->
    <div class="row">

      <div class="col-md-8">
        <img class="img-fluid" src="{{ $article->image_url_thumbnail }}" alt="">
      </div>

      <div class="col-md-4">
        <h3 class="my-3">Content</h3>
        <p>{{ $article->content }}</p>
        <h3 class="my-3">Category</h3>
        <p>{{ $article->category->name }}</p>
      </div>

    </div>
  </div>
@endsection
