<script>
    $(document).ready(function () {
        const url = "{{ route('admin.user.datatable') }}";
        const form = $('#form-user')
        const table = $('#dataTable');
        const modal = $('#modal-user')
        const btnCreate = $('.btn-create')
        const dataTable = table.DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'name', name: 'name'},
                {data: 'email', name: 'email'},
                {data: 'role.name', name: 'role.name'},
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        /* Functions */
        const saveUser = (data) => {
            const url = '{{ route("admin.user.store") }}'
            $.post(url, data, (res) => {
                clearModalForm()
                dataTable.ajax.reload();

                swalSuccess('Success');
            }).fail(err => {
                switch (err.status) {
                    case 422:
                        let message = err.responseJSON.data.join('<br>')
                        swalError(message)
                        break;
                    case 500:
                        swalError('Something went wrong')
                    default:
                        break;
                }
            })
        }

        const getUserById = (id) => {
            let url = "{{ route('admin.user.show', 'id') }}"
                url = url.replace('id', id)

            $.get(url, function (res) {
                modal.find('#id').val(res.data.id)
                modal.find('#name').val(res.data.name)
                modal.find('#email').val(res.data.email)
                modal.find('#role_id').val(res.data.role_id).trigger('change')
                modal.find('.form-password').hide()

                modal.modal('show')
                modal.find('.modal-title').text('Edit User')
                modal.find('.button-save').attr('data-type', 'update')
            })
        }

        const updateUser = (id, data) => {
            let url = "{{ route('admin.user.update', 'id') }}"
                url = url.replace('id', id)

            $.ajax({
                url: url,
                method: 'PUT',
                data: data,
                success: (res) => {
                    clearModalForm()
                    dataTable.ajax.reload();

                    swalSuccess('Success');
                },
                error: (err) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const deleteUser = (id) => {
            let url = "{{ route('admin.user.delete', 'id') }}"
                url = url.replace('id', id)

            $.ajax({
                url: url,
                method: 'DELETE',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: (res) => {
                    dataTable.ajax.reload();

                    swalSuccess('Success');
                },
                error: (err) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const clearModalForm = () => {
            modal.find('.form-control').val('')
            modal.modal('hide')
        }

        btnCreate.click(function () {
            modal.find('.form-control').val('')
            modal.find('.modal-title').text('Create User')
            modal.find('.button-save').attr('data-type', 'save')
            modal.find('.form-password').show()
            modal.modal('show')
        })

        form.submit(function (e) {
            e.preventDefault();

            const data = $(this).serialize()
            let id = modal.find('#id').val()

            if ($('.button-save').attr('data-type') == 'save') return saveUser(data)

            return updateUser(id, data)
        })

        $(document).on('click', '.btn-edit', function () {
            let self = $(this)
            let id = self.data('id')

            getUserById(id)
        })

        $(document).on('click', '.btn-delete', function () {
            if (confirm('Apakah anda yakin?')) {
                const id = $(this).data('id')

                deleteUser(id);
            }
        })
    });

</script>
