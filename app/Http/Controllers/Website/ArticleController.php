<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Services\ArticleService;
use Illuminate\Http\Request;

class ArticleController extends Controller
{
    protected $articleService;

    public function __construct(ArticleService $articleService)
    {
        $this->articleService = $articleService;
    }

    public function index()
    {
        $articles = $this->articleService->paginate(10);
        return view('web.article.index', compact('articles'));
    }

    public function show($slug)
    {
        $article = $this->articleService->findByColumn('slug', $slug);
        return view('web.article.detail', compact('article'));
    }
}
