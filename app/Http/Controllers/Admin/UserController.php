<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\JsonResponse;
use App\Http\Requests\UserRequest;
use App\Models\Role;
use App\Models\User;
use App\Services\UserService;
use DataTables;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class UserController extends Controller
{
    protected $userService;
    protected $jsonResponse;

    public function __construct(UserService $userService, JsonResponse $jsonResponse)
    {
        $this->userService = $userService;
        $this->jsonResponse = $jsonResponse;
    }

    public function index()
    {
        $roles = Role::all();
        return view('cms.user.index', compact('roles'));
    }

    public function store(UserRequest $request)
    {
        try {
            $this->userService->create($request->all());

            return $this->jsonResponse->data([])->message('Successfully create category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function update($id, UserRequest $request)
    {
        try {
            $this->userService->update($id, $request->all());

            return $this->jsonResponse->message('Successfully update category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function show($id)
    {
        try {
            $user = $this->userService->find($id);

            return $this->jsonResponse->data($user)->message('Successfully update category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            $this->userService->delete($id);

            return $this->jsonResponse->message('Successfully delete category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function datatable()
    {
        $users = $this->userService->get();

        return DataTables::of($users)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $disabled = '';
                if ($row->id == userLogin()->id) {
                    $disabled = 'disabled';
                }

                $btn = "<div class='d-flex justify-content-center align-items-center'>";
                $btn .= "<button class='btn btn-warning btn-edit btn-sm mr-2' data-id='{$row->id}'><i class='fa fa-pen' ></i></button>";
                $btn .= "<button data-id='{$row->id}' class='btn btn-danger btn-sm btn-delete' $disabled><i class='fa fa-trash'></i></button>";
                $btn .= '</div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function profile()
    {
        $profile = userLogin();
        return view('cms.user.profile', compact('profile'));
    }

    public function updateProfile($id, Request $request)
    {
        try {
            $validator = Validator::make($request->all(), [
                'name' => 'required',
                'email' => 'required|email|unique:users,email,' . $id,
                'password' => 'nullable|confirmed|min:6|required_with:old_password'
            ]);

            if ($validator->fails()) return back()->with('error', $validator->errors()->first());

            $oldPassword = userLogin()->password;

            if (!is_null($request['old_password']) && !Hash::check($request['old_password'], $oldPassword)) return back()->with('error', 'Old password not match');

            $this->userService->update($id, $request->all());

            return back()->with('success', 'Successfully update profile');
        } catch (\Throwable $th) {
            return back()->with('error', 'Internal Server Error');
        }
    }
}
