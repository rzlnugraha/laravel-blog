<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use \Illuminate\Contracts\Validation\Validator;
use \Illuminate\Validation\ValidationException;

class ArticleRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'title' => 'required',
            'content' => 'required',
            'banner' => 'nullable',
            'category_id' => 'required|exists:categories,id|numeric'
        ];
    }

    protected function failedValidation(Validator $validator)
    {
        $response = response()->json([
            'meta' => [
                'message' => 'something wrong !',
                'code' => 422,
            ],
            'data' => $validator->errors()->all(),
        ], 422);

        throw new ValidationException($validator, $response);
    }
}
