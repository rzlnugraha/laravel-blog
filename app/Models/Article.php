<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Article extends Model
{
    use HasFactory;

    protected $fillable = ['title', 'slug', 'content', 'banner', 'category_id', 'created_by'];
    protected $appends = ['image_url_original', 'image_url_thumbnail'];

    public function getImageUrlOriginalAttribute()
    {
        if (is_null($this->attributes['banner'])) return url('/images/default.png');

        return url('/images/article/' . $this->attributes['banner']);
    }

    public function getImageUrlThumbnailAttribute()
    {
        if (is_null($this->attributes['banner'])) return url('/images/default-thumbnail.jpg');

        return url('/images/thumbnail/article/' . $this->attributes['banner']);
    }

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'created_by', 'id');
    }
}
