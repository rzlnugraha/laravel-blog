<!-- Navigation-->
<nav class="navbar navbar-expand-lg navbar-light bg-light">
    <div class="container px-4 px-lg-5">
        <a class="navbar-brand" href="{{ route('homepage') }}">Laravel Blog</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><span class="navbar-toggler-icon"></span></button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0 ms-lg-4">
                <li class="nav-item"><a class="nav-link @if (Request::is('/')) active @endif" aria-current="page" href="{{ route('homepage') }}">Home</a></li>
                <li class="nav-item"><a class="nav-link @if (Request::is('article*')) active @endif" href="{{ route('article.index') }}">Articles</a></li>
                <li class="nav-item"><a class="nav-link @if (Request::is('category*')) active @endif" href="{{ route('category.index') }}">Category</a></li>
            </ul>
            @guest
            <form class="d-flex">
                <a class="btn btn-outline-dark" href="{{ route('login') }}">
                    <i class="bi bi-arrow-right-circle-fill"></i>
                    Login
                </a>
            </form>
            @else
            <a class="btn btn-outline-dark" href="{{ route('home') }}">
                <i class="bi bi-arrow-right-circle-fill"></i>
                CMS
            </a>
            @endguest
        </div>
    </div>
</nav>
