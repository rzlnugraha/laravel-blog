<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    const ROLE_ID_ADMIN = 1;
    const ROLE_ID_AUTHOR = 2;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Admin',
            'email' => 'admin@mailinator.com',
            'password' => bcrypt('password123'),
            'role_id' => self::ROLE_ID_ADMIN
        ]);

        User::create([
            'name' => 'Rizal Nugraha',
            'email' => 'rizal@mailinator.com',
            'password' => bcrypt('password123'),
            'role_id' => self::ROLE_ID_AUTHOR
        ]);
    }
}
