<?php

namespace Database\Seeders;

use App\Models\Article;
use Illuminate\Database\Seeder;
use Faker\Factory as Faker;
use Str;

class ArticleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $faker = Faker::create('id_ID');

        for ($i = 1; $i <= 20; $i++) {
            $title = $faker->sentence(3);
            Article::create([
                'title' => $title,
                'slug' => Str::slug($title),
                'content' => $faker->paragraph(),
                'banner' => null,
                'category_id' => rand(1, 10),
                'created_by' => rand(1,2)
            ]);
        }
    }
}
