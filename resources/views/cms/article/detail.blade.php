@extends('layouts.app')
@section('title')
    Laravel Blog - Article
@endsection

@push('css')
    <link href="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush


@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header py-3 d-flex align-items-center">
            <h6 class="m-0 font-weight-bold text-primary">{{ $article->title }}</h6>
        </div>
        <div class="card-body">
            <div class="content">
                <h6>Content : </h6>
                <p class="ml-3">{{ $article->content }}</p>
            </div>
            <div class="category">
                <h6>Category : </h6>
                <p class="ml-3">{{ $article->category->name }}</p>
            </div>
            <div class="image">
                <h6>Image</h6>
                <a target="_blank" href="{{ $article->image_url_original }}"><img src="{{ $article->image_url_original }}" alt="{{ $article->title }}"></a>
            </div>
            <hr>
            <p>Created At : {{ $article->created_at->format('Y-m-d H:i:s') }}</p>
        </div>
    </div>
</div>
@endsection

@push('js')
<script src="{{ asset('assets') }}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
@endpush
