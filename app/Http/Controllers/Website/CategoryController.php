<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    protected $categoryService;

    public function __construct(CategoryService $categoryService)
    {
        $this->categoryService = $categoryService;
    }

    public function index()
    {
        $categories = $this->categoryService->paginate(5);
        return view('web.category.index', compact('categories'));
    }

    public function show($slug)
    {
        $category = $this->categoryService->findByColumn('slug', $slug);
        return view('web.category.detail', compact('category'));
    }
}
