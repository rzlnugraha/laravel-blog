@extends('web.layouts.layouts')

@section('content')
<div class="container px-4 px-lg-5 mt-2">
    <div class="row gx-5 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-5 justify-content-center">
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>No</th>
                    <th>Name</th>
                    <th>#</th>
                </tr>
            </thead>
            <tbody>
                @forelse ($categories as $category)
                    <tr>
                        <td>{{ $loop->iteration }}</td>
                        <td>{{ $category->name }}</td>
                        <td><a href="{{ route('category.show', $category->slug) }}" class="btn btn-primary"> Browse Category</a></td>
                    </tr>
                @empty
                    <tr>
                        <td colspan="3"><center><b>Data not found</b></center></td>
                    </tr>
                @endforelse
            </tbody>
        </table>
        {{ $categories->links() }}
    </div>
</div>
@endsection
