<!-- Custom fonts for this template-->
<link href="{{ asset('assets') }}/vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
<link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

<!-- Custom styles for this template-->
<link href="{{ asset('assets') }}/css/sb-admin-2.min.css" rel="stylesheet">
<link href="{{ asset('assets') }}/vendor/toastr/toastr.min.css" rel="stylesheet">
<style>
    .select2 {
        width: 100% !important;
    }
    .select2-selection {
        height: 37px !important;
    }
    .select2-selection__rendered {
        line-height: 37px !important;
    }
    .select2-selection__arrow {
        top: 5px !important;
    }
</style>
@stack('css')
