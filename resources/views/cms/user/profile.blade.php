@extends('layouts.app')
@section('title')
    Laravel Blog - Profile {{ userLogin()->name }}
@endsection

@push('css')
    <link href="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.css" rel="stylesheet">
@endpush


@section('content')
<div class="container-fluid">
    <!-- Page Heading -->
    <div class="card shadow mb-4">
        <div class="card-header">
            <h6><b>Profile {{ $profile->name }}</b></h6>
        </div>
        <div class="card-body">
            <form method="POST" action="{{ route('admin.user.profile.update', $profile->id) }}">
                @csrf
                @method('PUT')
                <div class="row">
                    <div class="col-6">
                        <div class="form-group">
                          <label for="name">Full Name <span class="text-danger">*</span></label>
                          <input type="text" class="form-control" id="name" name="name" placeholder="Enter Full Name" value="{{ $profile->name }}" required>
                        </div>
                    </div>
                    <div class="col-6">
                        <div class="form-group">
                            <label for="email">Email address <span class="text-danger">*</span></label>
                            <input type="email" class="form-control" name="email" id="email" aria-describedby="emailHelp" placeholder="Enter email" value="{{ $profile->email }}" required>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-4">
                        <div class="form-group">
                          <label for="old_password">Old Password</label>
                          <input type="password" class="form-control" id="old_password" name="old_password" placeholder="Enter Old Password" @if (is_null($profile->password)) readonly @endif>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                          <label for="new_password">Password</label>
                          <input type="password" class="form-control" id="new_password" name="password" placeholder="Enter New Password">
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="form-group">
                          <label for="confirm_password">Confirm Password</label>
                          <input type="password" class="form-control" id="confirm_password" name="password_confirmation" placeholder="Enter Old Password">
                        </div>
                    </div>
                </div>
                <button type="submit" class="btn btn-primary">Submit</button>
            </form>
        </div>
    </div>
</div>


@endsection

@push('js')
<script src="{{ asset('assets') }}/vendor/datatables/jquery.dataTables.min.js"></script>
<script src="{{ asset('assets') }}/vendor/datatables/dataTables.bootstrap4.min.js"></script>
<script type="text/javascript" src="https://cdn.jsdelivr.net/momentjs/latest/moment.min.js"></script>
@include('cms.user.script')
@endpush
