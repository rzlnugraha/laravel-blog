@extends('web.layouts.layouts')

@section('content')
<div class="container px-4 px-lg-5 mt-2">
    <h6>Articles with Category {{ $category->name }}</h6>
    <hr>
    <table class="table table-striped">
        <thead>
            <tr>
                <th>No</th>
                <th>Name</th>
                <th>#</th>
            </tr>
        </thead>
        <tbody>
            @forelse ($category->articles as $article)
                <tr>
                    <td>{{ $loop->iteration }}</td>
                    <td>{{ $article->title }}</td>
                    <td></td>
                </tr>
            @empty
                <tr>
                    <td colspan="3"><center><b>Data not found</b></center></td>
                </tr>
            @endforelse
        </tbody>
    </table>
</div>
@endsection
