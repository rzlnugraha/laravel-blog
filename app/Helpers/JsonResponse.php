<?php
namespace App\Helpers;

class JsonResponse
{
    public $data = [];
    public $meta = [];
    public $message = '';
    public $errors = [];

    public function data($data)
    {
        $this->data = $data;
        return $this;
    }

    public function meta($meta)
    {
        $this->meta = $meta;
        return $this;
    }

    public function message($message)
    {
        $this->message = $message;
        return $this;
    }

    public function error($detail, $title = '', $code = 500)
    {
        $this->errors = [
            'detail' => $detail,
            'title' => $title,
            'code' => $code
        ];
        return $this;
    }

    public function success($status = 200)
    {
        $response = [];
        if ($this->meta != []) {
            $response['metadata'] = $this->meta;
            if ($this->message != '') {
                $response['metadata']['message'] = $this->message;
                $response['metadata']['code'] = $status;
            }
        } elseif ($this->message != '') {
            $response['metadata']['message'] = $this->message;
            $response['metadata']['code'] = $status;
        }

        $response['data'] = $this->data;

        return response($response, $status);
    }

    public function internalError($status = 500)
    {
        if ($this->errors == null) {
            $this->defaultError();
        }
        $response['errors'] = $this->errors;

        return response($response, $status);
    }

    public function clientError($status = 400)
    {
        if ($this->errors == null) {
            $this->defaultError();
        }
        $response['errors'] = $this->errors;

        return response($response, $status);
    }

    private function defaultError()
    {
        $this->errors = ['error' => 'Ups, something wrong here'];
    }

    public function json($data = [])
    {
        return response()->json($data);
    }
}
