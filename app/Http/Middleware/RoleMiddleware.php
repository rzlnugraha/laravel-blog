<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Auth;

class RoleMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse
     */
    public function handle(Request $request, Closure $next, ...$role)
    {
        if (Auth::guest()) {
            return redirect(route('login'));
        }

        if (!in_array(strtolower(optional($request->user()->role)->name), $role)) {
            return back()->with('error', 'Sorry, you dont have access');
        }

        return $next($request);
    }
}
