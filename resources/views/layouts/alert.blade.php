@if ($message = Session::get('success'))
<div class="container-fluid">
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <h5 class="alert-heading">Success !</h5>
        {{ $message }}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>
@endif

@if ($message = Session::get('error'))
<div class="container-fluid">
    <div class="alert alert-danger alert-dismissible fade show" role="alert">
        <h5 class="alert-heading">Error !</h5>
        {{ $message }}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>
@endif

@if ($message = Session::get('warning'))
<div class="container-fluid">
    <div class="alert alert-warning alert-dismissible fade show" role="alert">
        <h5 class="alert-heading">Warning !</h5>
        {{ $message }}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>
@endif

@if ($message = Session::get('info'))
<div class="container-fluid">
    <div class="alert alert-info alert-dismissible fade show" role="alert">
        <h5 class="alert-heading">Info !</h5>
        {{ $message }}
        <button class="close" type="button" data-dismiss="alert" aria-label="Close">
            <span aria-hidden="true">×</span>
        </button>
    </div>
</div>
@endif

@if ($errors->any())
<div class="m-3 alert alert-danger">
    <button type="button" class="close" data-dismiss="alert">×</button>
    {{ $errors->first() }}
</div>
@endif
