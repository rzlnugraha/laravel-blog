<script>
    $(document).ready(function () {
        const url = "{{ route('admin.article.datatable') }}";
        const form = $('#form-article')
        const table = $('#dataTable');
        const modal = $('#modal-article')
        const btnCreate = $('.btn-create')
        const dataTable = table.DataTable({
            processing: true,
            serverSide: true,
            ajax: url,
            columns: [
                {data: 'DT_RowIndex', name: 'DT_RowIndex'},
                {data: 'title', name: 'title'},
                {data: 'slug', name: 'slug'},
                {data: 'category.name', name: 'category.name'},
                {
                    data: 'user.name',
                    name: 'user.name',
                    render: function (data) {
                        return data ?? '-';
                    }
                },
                {data: 'action', name: 'action', orderable: false, searchable: false},
            ]
        });

        /* Functions */
        const saveArticle = (data) => {
            const url = '{{ route("admin.article.store") }}'

            $.ajax({
                url: url,
                type: 'POST',
                data: data,
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                success: function (res, status, xhr) {
                    clearModalForm()
                    modal.modal('hide')
                    dataTable.ajax.reload();

                    swalSuccess('Success');
                },
                error: (err, xhr) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                },
                cache: false,
                contentType: false,
                processData: false
            });
        }

        const getArticleById = (id) => {
            let url = "{{ route('admin.article.edit', 'id') }}"
                url = url.replace('id', id)

            $.get(url, function (res) {
                modal.find('#id').val(res.data.id)
                modal.find('#title').val(res.data.title)
                modal.find('#content').val(res.data.content)
                modal.find('#category_id').val(res.data.category_id).trigger('change')
                modal.find('.image-preview').show()
                modal.find('.img-preview').attr('src', res.data.image_url_thumbnail)

                modal.modal('show')
                modal.find('.modal-title').text('Edit Article')
                modal.find('.button-save').attr('data-type', 'update')
            })
        }

        const updateArticle = (id, data) => {
            let url = "{{ route('admin.article.update', 'id') }}"
                url = url.replace('id', id)

            data.append('_method', 'PUT');

            $.ajax({
                url: url,
                method: 'POST',
                headers: {
                    'X-CSRF-TOKEN': "{{ csrf_token() }}"
                },
                data: data,
                cache: false,
                contentType: false,
                processData: false,
                success: (res) => {
                    clearModalForm()
                    dataTable.ajax.reload();

                    modal.modal('hide')
                    swalSuccess('Success');
                },
                error: (err, xhr) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const deleteArticle = (id) => {
            let url = "{{ route('admin.article.delete', 'id') }}"
                url = url.replace('id', id)

            $.ajax({
                url: url,
                method: 'DELETE',
                data: {
                    _token: "{{ csrf_token() }}"
                },
                success: (res) => {
                    dataTable.ajax.reload();

                    modal.modal('hide')
                    swalSuccess('Success');
                },
                error: (err, xhr) => {
                    switch (err.status) {
                        case 422:
                            let message = err.responseJSON.data.join('<br>')
                            swalError(message)
                            break;
                        case 500:
                            swalError('Something went wrong')
                        default:
                            break;
                    }
                }
            })
        }

        const clearModalForm = () => {
            modal.find('.form-control').val('')
        }

        btnCreate.click(function () {
            clearModalForm()
            modal.find('.modal-title').text('Create Article')
            modal.find('.button-save').attr('data-type', 'save')
            modal.find('.img-preview').attr('src', '')
            modal.find('.image-preview').hide()

            modal.modal('show')

        })

        form.submit(function (e) {
            e.preventDefault();

            const data = new FormData(this);

            let id = modal.find('#id').val()

            if ($('.button-save').attr('data-type') == 'save') return saveArticle(data)

            return updateArticle(id, data)
        })

        $(document).on('click', '.btn-edit', function () {
            let self = $(this)
            let id = self.data('id')

            getArticleById(id)
        })

        $(document).on('click', '.btn-delete', function () {
            if (confirm('Apakah anda yakin?')) {
                const id = $(this).data('id')

                deleteArticle(id);
            }
        })
    });

</script>
