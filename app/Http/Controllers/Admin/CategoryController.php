<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Helpers\JsonResponse;
use App\Http\Requests\CategoryRequest;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use Yajra\DataTables\DataTables;
use Str;

class CategoryController extends Controller
{
    protected $categoryService;
    protected $jsonResponse;

    public function __construct(CategoryService $categoryService, JsonResponse $jsonResponse)
    {
        $this->categoryService = $categoryService;
        $this->jsonResponse = $jsonResponse;
    }

    public function index()
    {
        return view('cms.category.index');
    }

    public function store(CategoryRequest $request)
    {
        try {
            $this->categoryService->create([
                'name' => $request->name,
                'slug' => Str::slug($request->name)
            ]);

            return $this->jsonResponse->data([])->message('Successfully create category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function update($id, CategoryRequest $request)
    {
        try {
            $this->categoryService->update($id, [
                'name' => $request->name,
                'slug' => Str::slug($request->name)
            ]);

            return $this->jsonResponse->message('Successfully update category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function show($slug)
    {
        $category = $this->categoryService->findByColumn('slug', $slug);

        return view('cms.category.detail', compact('category'));
    }

    public function edit($id)
    {
        try {
            $category = $this->categoryService->find($id);

            return $this->jsonResponse->data($category)->message('Successfully update category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function delete($id)
    {
        try {
            $this->categoryService->delete($id);

            return $this->jsonResponse->message('Successfully delete category data')->success();
        } catch (\Throwable $th) {
            throw new \Exception($th->getMessage(), 500);
        }
    }

    public function datatable()
    {
        $categories = $this->categoryService->get();

        return DataTables::of($categories)
            ->addIndexColumn()
            ->addColumn('action', function ($row) {
                $browseCategory = route('admin.category.show', $row->slug);

                $btn = "<div class='d-flex justify-content-center align-items-center'>";
                $btn .= "<button title='Edit' class='btn btn-warning btn-edit btn-sm mr-2' data-id='{$row->id}'><i class='fa fa-pen'></i></button>";
                $btn .= "<a href='$browseCategory' title='Detail' class='btn btn-info btn-edit btn-sm mr-2'><i class='fa fa-info-circle'></i></a>";
                $btn .= "<button data-id='{$row->id}' title='Delete' class='btn btn-danger btn-sm btn-delete'><i class='fa fa-trash'></i></button>";
                $btn .= '</div>';
                return $btn;
            })
            ->rawColumns(['action'])
            ->make(true);
    }
}
