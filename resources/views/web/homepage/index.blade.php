@extends('web.layouts.layouts')

@section('content')
<div class="container px-4 px-lg-5 mt-2">
    <div class="row gx-5 gx-lg-5 row-cols-2 row-cols-md-3 row-cols-xl-5 justify-content-center">
        @if ($articles->isNotEmpty())
            @foreach ($articles as $article)
                <div class="col-4 mb-3">
                    <div class="card h-100">
                        <img class="card-img-top" src="{{ $article->image_url_thumbnail }}" alt="{{ $article->title }}" />
                        <div class="card-body p-4">
                            <div class="text-center">
                                <h5 class="fw-bolder">{{ $article->title }}</h5>
                                <small>{{ Str::limit($article->content, 20) }}</small>
                            </div>
                        </div>
                        <!-- Product actions-->
                        <div class="card-footer p-4 pt-0 border-top-0 bg-transparent">
                            <div class="text-center"><a class="btn btn-outline-dark mt-auto" href="{{ route('article.show', $article->slug) }}">Detail</a></div>
                        </div>
                    </div>
                </div>
            @endforeach
        @else
            <div class="text-center"><b>No Articles Found</b></div>
        @endif
    </div>
</div>
@endsection
