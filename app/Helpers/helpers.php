<?php

if (!function_exists('userLogin')) {
    function userLogin()
    {
        return auth()->user();
    }
}

if (!function_exists('isAdmin')) {
    function isAdmin()
    {
        $adminId = 1;

        return optional(auth()->user())->role_id == $adminId;
    }
}
