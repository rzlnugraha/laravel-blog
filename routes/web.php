<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\HomepageController::class, 'index'])->name('homepage');

/* Website */
Route::controller(App\Http\Controllers\Website\ArticleController::class)->group(function () {
    Route::prefix('article')->name('article.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/detail/{slug}', 'show')->name('show');
    });
});

Route::controller(App\Http\Controllers\Website\CategoryController::class)->group(function () {
    Route::prefix('category')->name('category.')->group(function () {
        Route::get('/', 'index')->name('index');
        Route::get('/detail/{slug}', 'show')->name('show');
    });
});

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::get('login/{provider}', [App\Http\Controllers\Auth\LoginController::class, 'redirectToProvider']);
Route::get('{provider}/callback', [App\Http\Controllers\Auth\LoginController::class, 'handleProviderCallback']);

/* CMS */
Route::middleware(['auth'])->prefix('admin')->name('admin.')->group(function () {
    /* Category */
    Route::controller(App\Http\Controllers\Admin\CategoryController::class)->middleware(['role:admin,author'])->group(function () {
        Route::prefix('category')->name('category.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/edit/{id}', 'edit')->name('edit');
            Route::get('/detail/{id}', 'show')->name('show');
            Route::get('/datatable', 'datatable')->name('datatable');
            Route::post('/', 'store')->name('store');
            Route::put('/{id}', 'update')->name('update');
            Route::delete('/delete/{id}', 'delete')->name('delete');
        });
    });

    /* Article */
    Route::controller(App\Http\Controllers\Admin\ArticleController::class)->middleware(['role:admin,author'])->group(function () {
        Route::prefix('article')->name('article.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/edit/{id}', 'edit')->name('edit');
            Route::get('/detail/{slug}', 'show')->name('detail');
            Route::get('/datatable', 'datatable')->name('datatable');
            Route::post('/', 'store')->name('store');
            Route::put('/{id}', 'update')->name('update');
            Route::delete('/delete/{id}', 'delete')->name('delete');
        });
    });

    /* User */
    Route::controller(App\Http\Controllers\Admin\UserController::class)->middleware(['role:admin'])->group(function () {
        Route::prefix('user')->name('user.')->group(function () {
            Route::get('/', 'index')->name('index');
            Route::get('/detail/{slug}', 'show')->name('show');
            Route::get('/datatable', 'datatable')->name('datatable');
            Route::post('/', 'store')->name('store');
            Route::put('/{id}', 'update')->name('update');
            Route::delete('/delete/{id}', 'delete')->name('delete');
        });
    });

    Route::put('user/profile/{id}', [App\Http\Controllers\Admin\UserController::class, 'updateProfile'])->name('user.profile.update');
    Route::get('user/profile', [App\Http\Controllers\Admin\UserController::class, 'profile'])->name('user.profile');
});
