@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-12">
            <div class="card">
                <div class="card-header">{{ __('Dashboard') }}</div>

                <div class="card-body">
                    <div class="text-center">
                        Hai {{ userLogin()->name }}! Welcome Back!
                    </div>
                </div>

                <div class="card-body">
                    <div class="ml-2"><h6><b>5 Latest Articles</b></h6></div>
                    <div class="d-flex justify-content-between">
                        @if ($articles->isNotEmpty())
                            @foreach ($articles as $article)
                                <figure class="figure m-2 border p-2 text-center">
                                    <a href="{{ route('admin.article.detail', $article->slug) }}"><img src="{{ $article->image_url_thumbnail }}" alt="{{ $article->title }}" class="figure-img img-fluid rounded" style="height: 100px;"></a>
                                    <figcaption class="figure-caption border-top">{{ $article->title }}</figcaption>
                                    <figcaption class="figure-caption border-top">{{ Str::limit($article->content, 20) }}</figcaption>
                                </figure>
                            @endforeach
                        @else
                            <div class="m-auto">Article Not Found</div>
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
