<!-- Bootstrap core JavaScript-->
<script src="{{ asset('assets') }}/vendor/jquery/jquery.min.js"></script>
<script src="{{ asset('assets') }}/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Core plugin JavaScript-->
<script src="{{ asset('assets') }}/vendor/jquery-easing/jquery.easing.min.js"></script>

<!-- Custom scripts for all pages-->
<script src="{{ asset('assets') }}/js/sb-admin-2.min.js"></script>

<!-- Page level plugins -->
<script src="{{ asset('assets') }}/vendor/chart.js/Chart.min.js"></script>
<script src="{{ asset('assets') }}/vendor/toastr/toastr.min.js"></script>
<script src="{{ asset('assets/vendor/pusher/pusher.min.js') }}"></script>
<script src="{{ asset('assets/vendor/sweetalert2/sweetalert.min.js') }}"></script>
<script>
    const loader = () => {
        Swal.fire({
            title: 'Please Wait !',
            allowOutsideClick: false,
            showConfirmButton: false,
            didOpen: () => {
                Swal.showLoading()
            },
        });
    }
    const closeLoader = () => Swal.close()

    const swalSuccess = message => {
        Swal.fire({
            icon: 'success',
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
    }

    const swalError = message => {
        Swal.fire({
            icon: 'error',
            title: message,
            showConfirmButton: false,
            timer: 1500
        })
    }
</script>
@stack('js')
