<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    <!-- Sidebar - Brand -->
    <a class="sidebar-brand d-flex align-items-center justify-content-center" href="{{ route('home') }}">
        <div class="sidebar-brand-icon rotate-n-15">
        </div>
        <div class="sidebar-brand-text mx-3">Laravel Blog</div>
    </a>

    <!-- Divider -->
    <hr class="sidebar-divider my-0">

    <!-- Nav Item - Dashboard -->
    <li class="nav-item @if (Request::is('home')) active @endif">
        <a class=" nav-link" href="{{ route('home') }}">
        <i class="fas fa-fw fa-tachometer-alt"></i>
        <span>Dashboard</span></a>
    </li>

    <li class="nav-item @if (Request::is('admin/article*')) active @endif">
        <a class=" nav-link" href="{{ route('admin.article.index') }}">
        <i class="fas fa-fw fa-newspaper"></i>
        <span>Article</span></a>
    </li>

    <li class="nav-item @if (Request::is('admin/category*')) active @endif">
        <a class=" nav-link" href="{{ route('admin.category.index') }}">
        <i class="fas fa-fw fa-list-alt"></i>
        <span>Category</span></a>
    </li>
    @if (isAdmin())
    <li class="nav-item @if (Request::is('admin/user*')) active @endif">
        <a class=" nav-link" href="{{ route('admin.user.index') }}">
        <i class="fas fa-fw fa-user"></i>
        <span>User</span></a>
    </li>
    @endif

    <!-- Divider -->
    <hr class="sidebar-divider">
    <li class="nav-item">
        <a class=" nav-link" href="{{ route('homepage') }}">
        <i class="fas fa-fw fa-arrow-right"></i>
        <span>Go To Website</span></a>
    </li>

</ul>
<!-- End of Sidebar -->
