<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Socialite;
use Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Redirect the user to the provider authentication page.
     *
     * @return Response
     */
    public function redirectToProvider($provider)
    {
        return Socialite::driver($provider)->redirect();
    }


    /**
     * Obtain the user information from provider.
     *
     * @return Response
     */
    public function handleProviderCallback($provider)
    {
        $social = Socialite::driver($provider)->user();

        $user = User::where('provider_id', $social->id)->first();

        if (!$user) {
            $user = User::create([
                'name' => $social->name ?? $social->nickname,
                'email' => $social->email,
                'provider_id' => $social->id,
                'provider' => $provider,
                'role_id' => 2 // Author
            ]);
        }

        Auth::login($user);

        return redirect()->route('home');
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string|min:6',
        ]);
        $login = [
            'email' => $request->email,
            'password' => $request->password
        ];
        if (auth()->attempt($login)) {
            return redirect()->route('home');
        }
        return redirect()->route('login')->withErrors(['password' => 'Email / Password salah!']);
    }
}
