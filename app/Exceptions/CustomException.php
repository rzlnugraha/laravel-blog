<?php

namespace App\Exceptions;

use App\Helpers\JsonResponse;
use Exception;

class CustomException extends Exception
{
    public function render($request)
    {
        if ($request->ajax()) {
            $response = resolve(JsonResponse::class);
            $response->error($this->getMessage(), 'Something went wrong');
            return $response->internalError();
        }

        return report($this->getMessage());
    }
}
